package config

import "flag"

var (
	flagConfigFile = flag.String("config", "", "Path to a Redlink configuration file.")
	flagSocketFile = flag.String("socket", "", "Path to Redlink socket.")
)

func flagInit() {
	flag.Parse()
}

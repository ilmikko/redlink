package config

import (
	"fmt"
	"log"
	"strings"
)

func (cfg *Config) parseFConfig(cmd string, fields []string) error {
	fields, err := cfg.fieldsSplitGrab0(cmd, fields)
	if err != nil {
		return err
	}

	l := len(fields)
	context, file := fields[:l-1], fields[l-1]
	if err := cfg.Load(file, context...); err != nil {
		return fmt.Errorf("CONFIG %q: %v", fields, err)
	}
	return nil
}

func (cfg *Config) parseFHostAuth(cmd string, fields []string) error {
	id, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	key, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Hosts[id].AuthKey = key
	return nil
}

func (cfg *Config) parseFHostConfigPort(cmd string, fields []string) error {
	id, port, _, err := cfg.fieldsSplitGrab2(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Hosts[id].Port = port
	return nil
}

func (cfg *Config) parseFHostConfigUser(cmd string, fields []string) error {
	id, user, _, err := cfg.fieldsSplitGrab2(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Hosts[id].User = user
	return nil
}

func (cfg *Config) parseFHostConfig(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "PORT":
		return cfg.parseFHostConfigPort(cmd, fields)
	case "USER":
		return cfg.parseFHostConfigUser(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: PORT, USER")
	}
}

func (cfg *Config) parseFHostDefine(cmd string, fields []string) error {
	id, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	hostname := id
	if len(fields) > 0 {
		hostname = fields[0]
	}

	if _, ok := cfg.Hosts[id]; ok {
		log.Printf("Double definition for host id %q, ignoring...", id)
		return nil
	}
	cfg.Hosts[id] = NewHost(id, hostname)
	return nil
}

func (cfg *Config) parseFHostRemove(cmd string, fields []string) error {
	id, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	return cfg.RemoveHost(id)
}

func (cfg *Config) parseFHostSelfPort(cmd string, fields []string) error {
	port, _, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	cfg.Host.Port = port
	return nil
}

func (cfg *Config) parseFHostSelf(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "PORT":
		return cfg.parseFHostSelfPort(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: PORT")
	}
}

func (cfg *Config) parseFHostTrust(cmd string, fields []string) error {
	id, _, kt, fields, err := cfg.fieldsSplitGrab3(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Hosts[id].AddHostKey(kt, strings.Join(fields, " "))
	return nil
}

func (cfg *Config) parseFHost(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "AUTHORIZE", "AUTH":
		// Authorize a host in authorized_keys
		return cfg.parseFHostAuth(cmd, fields)
	case "CONFIG":
		// Change host configuration from the default
		return cfg.parseFHostConfig(cmd, fields)
	case "DEFINE":
		return cfg.parseFHostDefine(cmd, fields)
	case "REMOVE":
		return cfg.parseFHostRemove(cmd, fields)
	case "SELF":
		// Localhost configuration
		return cfg.parseFHostSelf(cmd, fields)
	case "TRUST":
		// Explicitly trust a host key in known_hosts
		return cfg.parseFHostTrust(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: AUTH/AUTHORIZE, CONFIG, DEFINE, REMOVE, SELF, TRUST")
	}
}

func (cfg *Config) parseFKeyAuthKeys(cmd string, fields []string) error {
	akf, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.Keys.authKeysFile = akf
	return nil
}

func (cfg *Config) parseFKeyDir(cmd string, fields []string) error {
	dir, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.Keys.dir = dir
	return cfg.Validate()
}

func (cfg *Config) parseFKeyKnownHosts(cmd string, fields []string) error {
	khf, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.Keys.knownHostsFile = khf
	return nil
}

func (cfg *Config) parseFKeyPrivate(cmd string, fields []string) error {
	pf, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.Keys.privFile = pf
	return nil
}

func (cfg *Config) parseFKey(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "AUTHORIZED_KEYS", "AUTH_KEYS":
		return cfg.parseFKeyAuthKeys(cmd, fields)
	case "DIR":
		return cfg.parseFKeyDir(cmd, fields)
	case "KNOWN_HOSTS":
		return cfg.parseFKeyKnownHosts(cmd, fields)
	case "PRIVATE":
		return cfg.parseFKeyPrivate(cmd, fields)
	case "SHOW":
		return cfg.unknown(cmd, "SHOW should only be used via the socket.")
	default:
		return cfg.unknown(cmd, "Valid fields: AUTH_KEYS/AUTHORIZED_KEYS, DIR, KNOWN_HOSTS, PRIVATE, SHOW")
	}
}

func (cfg *Config) parseFLinkDir(cmd string, fields []string) error {
	id, remote, local, _, err := cfg.fieldsSplitGrab3(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Link.Dir = append(cfg.Link.Dir, NewLink(cfg.Hosts[id], local, remote))
	return nil
}

func (cfg *Config) parseFLinkPort(cmd string, fields []string) error {
	id, remote, local, _, err := cfg.fieldsSplitGrab3(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Link.Port = append(cfg.Link.Port, NewLink(cfg.Hosts[id], local, remote))
	return nil
}

// TODO: REVERSE DIR link is essentially similar, can we reuse the code?
func (cfg *Config) parseFLinkReverseDir(cmd string, fields []string) error {
	id, remote, local, _, err := cfg.fieldsSplitGrab3(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Link.ReverseDir = append(cfg.Link.ReverseDir, NewLink(cfg.Hosts[id], local, remote))
	return nil
}

func (cfg *Config) parseFLinkReversePort(cmd string, fields []string) error {
	id, remote, local, _, err := cfg.fieldsSplitGrab3(cmd, fields)
	if err != nil {
		return err
	}

	if err := cfg.unlockHost(id); err != nil {
		return err
	}

	cfg.Link.ReversePort = append(cfg.Link.ReversePort, NewLink(cfg.Hosts[id], local, remote))
	return nil
}

func (cfg *Config) parseFLinkReverse(cmd string, fields []string) error {
	// All of the reverse links need to go through SSH first, then execute a command operating on a reverse port back to this host.
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "DIR":
		return cfg.parseFLinkReverseDir(cmd, fields)
	case "PORT":
		return cfg.parseFLinkReversePort(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: DIR")
	}
}

func (cfg *Config) parseFLink(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "DIR":
		//Link a directory with SSHFS. This will also be health checked periodically.
		return cfg.parseFLinkDir(cmd, fields)
	case "PORT":
		// Link a port via SSH. The SSH connection is health checked via an available port.
		return cfg.parseFLinkPort(cmd, fields)
	case "REVERSE":
		// Reverse linking (using an arbitrary SSH port with -R)
		return cfg.parseFLinkReverse(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: DIR, PORT, REVERSE")
	}
}

func (cfg *Config) parseFSocket(cmd string, fields []string) error {
	sock, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.SocketFile = sock
	return nil
}

func (cfg *Config) parseFUserBin(cmd string, fields []string) error {
	bin, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.User.bin = bin
	return cfg.Validate()
}

func (cfg *Config) parseFUserSSHFS(cmd string, fields []string) error {
	options, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.User.SSHFSOptions = strings.Split(options, " ")
	return nil
}

func (cfg *Config) parseFUserName(cmd string, fields []string) error {
	user, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.User.Name = user
	return cfg.Validate()
}

func (cfg *Config) parseFUserProfile(cmd string, fields []string) error {
	profileFile, err := cfg.fieldsSplitGrabAll(cmd, fields)
	if err != nil {
		return err
	}

	cfg.User.profile = profileFile
	return cfg.Validate()
}

func (cfg *Config) parseFUser(cmd string, fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1(cmd, fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "BIN":
		return cfg.parseFUserBin(cmd, fields)
	case "NAME":
		return cfg.parseFUserName(cmd, fields)
	case "PROFILE":
		return cfg.parseFUserProfile(cmd, fields)
	case "SSHFS":
		return cfg.parseFUserSSHFS(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: BIN, NAME, PROFILE")
	}
}

func (cfg *Config) parseF(fields []string) error {
	cmd, fields, err := cfg.fieldsSplitGrab1("", fields)
	if err != nil {
		return err
	}

	switch strings.ToUpper(cmd) {
	case "CONFIG":
		return cfg.parseFConfig(cmd, fields)
	case "HOST":
		return cfg.parseFHost(cmd, fields)
	case "KEY":
		return cfg.parseFKey(cmd, fields)
	case "LINK":
		return cfg.parseFLink(cmd, fields)
	case "RELOAD":
		return cfg.unknown(cmd, "RELOAD rule should only be used via the socket.")
	case "SOCKET":
		return cfg.parseFSocket(cmd, fields)
	case "STATUS":
		return cfg.unknown(cmd, "STATUS rule should only be used via the socket.")
	case "USER":
		return cfg.parseFUser(cmd, fields)
	default:
		return cfg.unknown(cmd, "Valid fields: CONFIG, HOST, KEY, LINK, RELOAD, SOCKET, STATUS, USER")
	}
}

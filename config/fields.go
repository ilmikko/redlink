package config

import (
	"fmt"
	"strings"
)

const (
	COMMENT_RUNE = '#'
)

func (cfg *Config) fieldsSplitGrab3(cmd string, fields []string) (string, string, string, []string, error) {
	if len(fields) < 3 {
		return "", "", "", nil, fmt.Errorf("%s needs %d field(s). (provided %d)", cmd, 3, len(fields))
	}

	return fields[0], fields[1], fields[2], fields[3:], nil
}

func (cfg *Config) fieldsSplitGrab2(cmd string, fields []string) (string, string, []string, error) {
	if len(fields) < 2 {
		return "", "", nil, fmt.Errorf("%s needs %d field(s). (provided %d)", cmd, 2, len(fields))
	}

	return fields[0], fields[1], fields[2:], nil
}

func (cfg *Config) fieldsSplitGrab1(cmd string, fields []string) (string, []string, error) {
	if len(fields) == 0 {
		return "", nil, fmt.Errorf("%s needs additional fields.", cmd)
	}

	return fields[0], fields[1:], nil
}

// Useful for ensuring there are fields left.
func (cfg *Config) fieldsSplitGrab0(cmd string, fields []string) ([]string, error) {
	if len(fields) == 0 {
		return nil, fmt.Errorf("%s needs additional fields.", cmd)
	}
	return fields, nil
}

func (cfg *Config) fieldsSplitGrabAll(cmd string, fields []string) (string, error) {
	if len(fields) == 0 {
		return "", fmt.Errorf("%s needs additional fields.", cmd)
	}

	return strings.Join(fields, " "), nil
}

func (cfg *Config) unknown(cmd string, help ...string) error {
	if len(help) == 0 {
		return fmt.Errorf("Unknown field: %q", cmd)
	}

	return fmt.Errorf("Unknown field: %q (%s)", cmd, strings.Join(help, "\n"))
}

func (cfg *Config) IsRule(line string) bool {
	fields := strings.Fields(line)

	// Empty line.
	if len(fields) == 0 {
		return false
	}

	// Comment.
	if fields[0][0] == COMMENT_RUNE {
		return false
	}

	return true
}

func (cfg *Config) ParseLine(line string, context ...string) error {
	fields := strings.Fields(line)

	if !cfg.IsRule(line) {
		return nil
	}

	// Additional context.
	fields = append(context, fields...)
	if err := cfg.parseF(fields); err != nil {
		return fmt.Errorf("In line %q: %v", line, err)
	}
	return nil
}

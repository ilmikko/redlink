package config

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
)

const (
	DEFAULT_KEY_AUTH_KEYS_FILE   = "./authorized_keys"
	DEFAULT_KEY_DIR              = ".ssh"
	DEFAULT_KEY_KNOWN_HOSTS_FILE = "./known_hosts"
	DEFAULT_KEY_PRIV_FILE        = "./link.key"
	DEFAULT_PORT                 = "22"
	DEFAULT_SOCKET_FILE          = "/tmp/redlink.sock"
	DEFAULT_USER_NAME            = "link"
	DEFAULT_USER_BIN             = ".local/bin"
	DEFAULT_USER_PROFILE         = ".profile"
)

type Host struct {
	Id   string
	Name string
	Port string
	User string
	// A host is considered "Active" if they are in any link configurations.
	// Non-active hosts can be useful if they need to be AUTHORIZED or TRUSTED
	// but not linked directly by Redlink.
	Active bool
	// If a host is "locked", its configuration cannot be changed anymore.
	// All hosts that are defined in a configuration file will become locked at startup.
	// Any locked hosts cannot be changed or deleted without a daemon restart.
	Locked   bool
	AuthKey  string
	HostKeys map[string]string
}

func (h *Host) AddHostKey(kt, key string) error {
	if old, ok := h.HostKeys[kt]; ok {
		if old == key {
			return nil
		}

		return fmt.Errorf("Host key mismatch! Old %q != New %q", key, old)
	}

	h.HostKeys[kt] = key
	return nil
}

func NewHost(id, name string) *Host {
	h := &Host{
		Id:       id,
		Name:     name,
		Port:     DEFAULT_PORT,
		User:     DEFAULT_USER_NAME,
		HostKeys: map[string]string{},
		Active:   false,
		Locked:   false,
	}

	return h
}

type Link struct {
	Host   *Host
	Local  string
	Remote string
}

func NewLink(host *Host, local, remote string) *Link {
	l := &Link{
		Host:   host,
		Local:  local,
		Remote: remote,
	}

	host.Active = true
	return l
}

func concatIfNotAbsolute(dir, path string) string {
	if path[0] == '/' {
		return path
	}
	return fmt.Sprintf("%s/%s", dir, path)
}

type User struct {
	Home         string
	bin          string
	profile      string
	Name         string
	SSHFSOptions []string // TODO: move to "custom" etc
}

func (u *User) Bin() string {
	return concatIfNotAbsolute(u.Home, u.bin)
}

func (u *User) Profile() string {
	return concatIfNotAbsolute(u.Home, u.profile)
}

type Keys struct {
	home           string
	dir            string
	authKeysFile   string
	knownHostsFile string
	privFile       string
}

func (k *Keys) Dir() string {
	return concatIfNotAbsolute(k.home, k.dir)
}

func (k *Keys) AuthKeysFile() string {
	return concatIfNotAbsolute(k.Dir(), k.authKeysFile)
}

func (k *Keys) KnownHostsFile() string {
	return concatIfNotAbsolute(k.Dir(), k.knownHostsFile)
}

func (k *Keys) PrivFile() string {
	return concatIfNotAbsolute(k.Dir(), k.privFile)
}

func (k *Keys) PubFile() string {
	return concatIfNotAbsolute(k.Dir(), k.privFile+".pub")
}

type Config struct {
	Hosts map[string]*Host
	Keys  Keys
	Link  struct {
		Dir         []*Link
		ReverseDir  []*Link
		ReversePort []*Link
		Port        []*Link
	}
	User       User
	Host       Host
	SocketFile string
}

func (cfg *Config) hasHost(host string) error {
	if _, ok := cfg.Hosts[host]; !ok {
		return fmt.Errorf("Unknown host: %q", host)
	}

	return nil
}

func (cfg *Config) unlockHost(host string) error {
	if err := cfg.hasHost(host); err != nil {
		return err
	}

	if cfg.Hosts[host].Locked {
		return fmt.Errorf("Host %q is locked.", host)
	}
	return nil
}

func (cfg *Config) LockHosts() {
	for _, h := range cfg.Hosts {
		h.Locked = true
	}
}

func (cfg *Config) RemoveHost(host string) error {
	if err := cfg.hasHost(host); err != nil {
		return err
	}

	if err := cfg.unlockHost(host); err != nil {
		return err
	}

	delete(cfg.Hosts, host)
	return nil
}

func (cfg *Config) Status() []string {
	lines := []string{}

	activeHosts := 0
	for _, h := range cfg.Hosts {
		if h.Active {
			activeHosts++
		}
	}

	for id, host := range cfg.Hosts {
		flags := ""
		if host.Active {
			flags += "[*]" // Active host
		}
		if host.Locked {
			flags += "[L]" // Locked host
		}
		if host.AuthKey != "" {
			flags += "[AK]" // Authorized Key
		}
		if l := len(host.HostKeys); l > 0 {
			flags += fmt.Sprintf("[%dET]", l) // Explicitly Trusted host keys
		}
		lines = append(lines, fmt.Sprintf("%s %s (%s) %s",
			id,
			host.Name,
			fmt.Sprintf("%s@%s:%s",
				host.User,
				host.Name,
				host.Port,
			),
			flags,
		))
	}

	lines = append(lines, fmt.Sprintf("%d host(s) with active rules [*]", activeHosts))

	return lines
}

func (cfg *Config) Validate() error {
	// Retrieve user information.
	{
		u, err := user.Lookup(cfg.User.Name)
		if err != nil {
			return err
		}

		cu, err := user.Current()
		if err != nil {
			return err
		}

		if cu.Uid == "0" {
			return fmt.Errorf("Please do not run Redlink as root! Instead, create a new user (such as `link`) to run as.")
		}

		if cu.Uid != u.Uid {
			return fmt.Errorf(
				"Redlink is not running as the configured user: %s (uid %q) != %s (uid %q)",
				cu.Username, cu.Uid, u.Username, u.Uid,
			)
		}

		// Required an absolute path by SSHFS.
		home, err := filepath.Abs(u.HomeDir)
		if err != nil {
			return err
		}
		cfg.User.Home = home
		cfg.Keys.home = home
	}

	// Check that key dir exists.
	{
		dir := cfg.Keys.Dir()
		if f, err := os.Stat(dir); err != nil || !f.IsDir() {
			if err != nil {
				return err
			}
			return fmt.Errorf("Expected %q to be a directory, but it is not!", dir)
		}
	}
	return nil
}

func Init() (*Config, error) {
	cfg := &Config{}

	cfg.Hosts = map[string]*Host{}

	localhost, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	cfg.Host = *NewHost("", localhost)

	cfg.Keys.dir = DEFAULT_KEY_DIR

	cfg.Keys.authKeysFile = DEFAULT_KEY_AUTH_KEYS_FILE
	cfg.Keys.knownHostsFile = DEFAULT_KEY_KNOWN_HOSTS_FILE
	cfg.Keys.privFile = DEFAULT_KEY_PRIV_FILE

	cfg.SocketFile = DEFAULT_SOCKET_FILE

	cfg.User.Name = DEFAULT_USER_NAME
	cfg.User.profile = DEFAULT_USER_PROFILE
	cfg.User.bin = DEFAULT_USER_BIN

	// TODO: These may not be needed
	cfg.Link.Dir = []*Link{}
	cfg.Link.Port = []*Link{}
	cfg.Link.ReverseDir = []*Link{}
	cfg.Link.ReversePort = []*Link{}

	flagInit()

	if f := *flagConfigFile; f != "" {
		if err := cfg.Load(f); err != nil {
			return nil, err
		}
	}

	if f := *flagSocketFile; f != "" {
		cfg.SocketFile = f
	}

	if err := cfg.Validate(); err != nil {
		return nil, err
	}

	cfg.LockHosts()
	return cfg, nil
}

package config

import (
	"bufio"
	"os"
)

func (cfg *Config) Load(file string, context ...string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		if err := cfg.ParseLine(s.Text(), context...); err != nil {
			return err
		}
	}

	return nil
}

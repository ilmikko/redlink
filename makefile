GITPATH=gitlab.com/ilmikko/redlink

PACKAGES=./config \
				 ./common/port \
				 ./common/ssh \
				 ./redlink \
				 ./redlink/keys \
				 ./redlink/keys/authkeys \
				 ./redlink/keys/knownhosts \
				 ./redlink/port \
				 ./redlink/reversedir \

# We fake a GOPATH for people that just want to build the module.
build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/redlink redlink-cli.go
	GOPATH="/tmp/go" go build -o bin/redlinkd redlink.go
	@rm -rf /tmp/go

test:
	go test $(PACKAGES)

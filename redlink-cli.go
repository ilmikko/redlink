package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
)

const (
	STATUS_OK = "OK\n"
)

var (
	redlinkSocket = flag.String("socket", "/tmp/redlink.sock", "Socket to connect to Redlink.")
)

func mainErr() error {
	flag.Parse()

	conn, err := net.Dial("unix", *redlinkSocket)
	if err != nil {
		return err
	}

	r := bufio.NewReader(conn)
	fmt.Fprintf(conn, "%s\n", strings.Join(flag.Args(), " "))

	status, err := r.ReadString('\n')
	if err == io.EOF {
		return fmt.Errorf("EOF before status message!")
	}
	if err != nil {
		return err
	}

	for {
		data, err := r.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		fmt.Printf("%s", data)
	}

	if status != STATUS_OK {
		return fmt.Errorf("%v", status)
	}

	return nil
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("ERROR: %v", err)
	}
}

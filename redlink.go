package main

import (
	"fmt"
	"log"

	"gitlab.com/ilmikko/redlink/config"
	"gitlab.com/ilmikko/redlink/redlink"
)

func mainErr() error {
	cfg, err := config.Init()
	if err != nil {
		return fmt.Errorf("Config: %v", err)
	}

	r, err := redlink.New(cfg)
	if err != nil {
		return fmt.Errorf("Init: %v", err)
	}
	defer r.Clean()

	if err := r.Reload(); err != nil {
		return err
	}

	return r.Listen()
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("ERROR: %v", err)
	}
}

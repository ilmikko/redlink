# Redlink

Redesigned uplink.
Better support for cases like reverse tunneling, single-linked hosts, filesystem linking and port forwarding.
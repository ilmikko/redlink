package port

import (
	"fmt"
	"math/rand"
	"time"
)

type Port struct {
	rand *rand.Rand
}

// There should be an available port somewhere in the range 30000..50000.
// TODO: Maybe we could be smarter about this.
func (p *Port) GetProbablyAvailablePort() string {
	return fmt.Sprintf("%d", 30000+(p.rand.Int()%20000))
}

func New() *Port {
	p := &Port{}
	p.rand = rand.New(rand.NewSource(time.Now().UnixNano()))

	return p
}

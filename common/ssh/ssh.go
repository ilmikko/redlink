package ssh

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"gitlab.com/ilmikko/redlink/config"
)

const (
	// How long, roughly, do we wait before declaring a new connection dead?
	SSH_CONNECT_TIMEOUT = 5 * time.Second
	// How many health checks should we make in that time?
	SSH_CONNECT_TRIES = 5
	// How long from the last health "OK" before we deem the connection dead?
	SSH_HEALTH_TIMEOUT = 5 * time.Minute
	// How long to wait before starting health check?
	SSH_INIT_WAIT = 5 * time.Second
)

var (
	IgnoredSTDERR = []*regexp.Regexp{
		// Warning: Permanently added the * host key for IP address '*' to the list of known hosts.
		// This is not useful to us; DNS resolving to an IP address is not something we are concerned about.
		regexp.MustCompile("Warning: Permanently added the \\S+ host key for IP address '\\S+' to the list of known hosts."),
	}
)

type SSH struct {
	Cmd         *exec.Cmd
	Commands    []string
	Options     []string
	Host        *config.Host
	PrivFile    string
	Stderr      *bytes.Buffer
	Stdout      *bytes.Buffer
	LastHealthy time.Time
}

// Generates output such as: cmd1 && cmd2 && cmd3 &&
// The health checking command will be ran last - this means if a command in the chain fails,
// then we abort as we never get the OK.
// TODO: What if we want to run background processes?
func (s *SSH) CommandsString() []string {
	st := []string{}
	for _, c := range s.Commands {
		st = append(st, c, "&&")
	}
	return st
}

func (s *SSH) Connect() error {
	h := s.Host
	log.Printf("SSH connecting to %q (%s)...", h.Id, h.Name)
	s.Disconnect()

	s.Cmd = exec.Command(
		"ssh",
		append(
			[]string{
				fmt.Sprintf("%s@%s", h.User, h.Name),
				"-i", s.PrivFile,
				"-p", h.Port,
			},
			append(
				s.Options,
				append(
					s.CommandsString(),
					"while true; do echo OK; sleep 10; done", // Send data back for health checking connection
				)...,
			)...,
		)...,
	)

	var o, e bytes.Buffer
	s.Cmd.Stderr = &e
	s.Cmd.Stdout = &o
	s.Stderr = &e
	s.Stdout = &o

	if err := s.Cmd.Start(); err != nil {
		return fmt.Errorf("Connect start: %v", err)
	}

	time.Sleep(SSH_INIT_WAIT)

	// We will check that host actually connects.
	for i := 0; i < SSH_CONNECT_TRIES; i++ {
		if s.Healthy() {
			break
		}
		time.Sleep(SSH_CONNECT_TIMEOUT / SSH_CONNECT_TRIES)
	}

	if !s.Healthy() {
		return fmt.Errorf("Host %q did not connect in time. STDERR: %s", h.Id, s.Stderr.String())
	}

	log.Printf("Host %q connected successfully.", h.Id)
	return nil
}

func (s *SSH) Disconnect() error {
	if s.Cmd == nil {
		return nil
	}
	if err := s.Cmd.Process.Kill(); err != nil {
		return err
	}
	return nil
}

func (s *SSH) checkSTDERR() bool {
	// Output to STDERR is usually registered unhealthy.
	// There are some exceptions, check them here.
	errStr := s.Stderr.String()
	if errStr == "" {
		return true
	}

	for _, r := range IgnoredSTDERR {
		if r.MatchString(errStr) {
			log.Printf("Ignored %q in STDERR", errStr)
			s.Stderr.Reset()
			return true
		}
	}

	return false
}

func (s *SSH) Healthy() bool {
	if !s.checkSTDERR() {
		return false
	}

	// Check health reports through STDOUT.
	// We will receive an "OK" every n seconds.
	// This means the buffer can contain "", "OK\n", "OK\nOK\n", ...
	out := s.Stdout.String()
	s.Stdout.Reset()

	// If the buffer did not have any "OK", we will need to see if we got an "OK" previously within a reasonable amount of time.
	if out == "" {
		delta := time.Now().Sub(s.LastHealthy)
		return delta < SSH_HEALTH_TIMEOUT
	}

	// If we encounter any number of "OK", we register healthy.
	// It's fine to be a bit fuzzy here as the buffer gets cleared in any case.
	if !strings.Contains(out, "OK") {
		return false
	}

	s.LastHealthy = time.Now()
	return true
}

func (s *SSH) Run(cmds ...string) (string, error) {
	c := exec.Command(
		"ssh",
		append(
			[]string{
				fmt.Sprintf("%s@%s", s.Host.User, s.Host.Name),
				"-i", s.PrivFile,
			},
			append(
				s.Options,
				cmds...,
			)...,
		)...,
	)

	var o, e bytes.Buffer
	c.Stderr = &e
	c.Stdout = &o

	if err := c.Run(); err != nil {
		return "", fmt.Errorf("Run: %v: %s", err, e.String())
	}

	return o.String(), nil
}

func New(privfile string, host *config.Host) *SSH {
	s := &SSH{}
	s.Host = host
	s.PrivFile = privfile
	return s
}

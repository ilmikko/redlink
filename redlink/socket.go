package redlink

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"

	"gitlab.com/ilmikko/redlink/config"
)

type Socket struct {
	CFG      *config.Config
	File     string
	Listener net.Listener
	Redlink  *Redlink
}

func (s *Socket) handleConnectionErr(conn net.Conn) error {
	r := bufio.NewReader(conn)

	line, err := r.ReadString('\n')
	if err != nil {
		return err
	}

	if s.CFG.IsRule(line) {
		// Special commands that affect Redlink instead of the config.
		st := strings.Trim(line, " \t\n")
		switch strings.ToUpper(st) {
		case "KEY SHOW":
			pub, err := s.Redlink.Keys.Public()
			if err != nil {
				return err
			}
			fmt.Fprintf(conn, "%s\n", "OK")
			fmt.Fprintf(conn, "%s\n", pub)
			return nil
		case "RELOAD":
			if err := s.Redlink.Reload(); err != nil {
				return err
			}
			fmt.Fprintf(conn, "%s\n", "OK")
			return nil
		case "STATUS":
			st := s.Redlink.Status()
			fmt.Fprintf(conn, "%s\n", "OK")
			fmt.Fprintf(conn, "%s\n", strings.Join(st, "\n"))
			return nil
		}
	}

	if err := s.CFG.ParseLine(line); err != nil {
		return err
	}
	fmt.Fprintf(conn, "%s\n", "OK")
	return nil
}

func (s *Socket) handleConnection(conn net.Conn) {
	defer conn.Close()
	if err := s.handleConnectionErr(conn); err != nil {
		fmt.Fprintf(conn, "FAIL %s\n", err.Error())
	}
}

func (s *Socket) Listen() error {
	for {
		conn, err := s.Listener.Accept()
		if err != nil {
			return err
		}
		go s.handleConnection(conn)
	}
}

func NewSocket(cfg *config.Config, rl *Redlink) (*Socket, error) {
	s := &Socket{CFG: cfg}

	// Remove any previous socket file.
	os.Remove(cfg.SocketFile)

	listener, err := net.Listen("unix", cfg.SocketFile)
	if err != nil {
		return nil, fmt.Errorf("Listen: %v", err)
	}
	s.Listener = listener
	s.Redlink = rl

	return s, nil
}

package reversedir

import (
	"fmt"

	"gitlab.com/ilmikko/redlink/common/port"
	"gitlab.com/ilmikko/redlink/common/ssh"
	"gitlab.com/ilmikko/redlink/config"
)

type ReverseDir struct {
	port *port.Port
	CFG  *config.Config
	SSHs map[string]*ssh.SSH
}

func (r *ReverseDir) Clean() {
	if r.SSHs != nil {
		for _, s := range r.SSHs {
			s.Disconnect()
		}
	}
	r.SSHs = map[string]*ssh.SSH{}
}

func (r *ReverseDir) ConnectToHosts() error {
	for _, s := range r.SSHs {
		if err := s.Connect(); err != nil {
			return err
		}
	}
	return nil
}

func (r *ReverseDir) Link(l *config.Link) error {
	if _, ok := r.SSHs[l.Host.Id]; !ok {
		r.SSHs[l.Host.Id] = ssh.New(r.CFG.Keys.PrivFile(), l.Host)
	}

	// I am aware that SSH allows you to pick an arbitrary port by specifying `0`.
	// The problem is, it makes the rest of the code so much more complicated as now I have to
	// go through the STDOUT of the SSH command to find the allocated port after connection,
	// before running any of my other commands.
	// It's much simpler to throw a dice and fail fast if that port happens to be unavailable.
	// The probability of this happening multiple times in a row is astronomically low.
	arbitraryPort := r.port.GetProbablyAvailablePort()

	// TODO: We should name the host randomly (to avoid conflicts), or even have Redlink understand
	// that this is an ephemeral host that will die with this connection.
	arbitraryHost := "ephemeral-test"

	s := r.SSHs[l.Host.Id]
	s.Options = append(s.Options, "-R",
		fmt.Sprintf("%s:localhost:%s", arbitraryPort, r.CFG.Host.Port),
	)

	// TODO: Retrieve public key from redlink.
	// This means we don't have to TRUST the host, but can do ephemeral trust for the time being.
	// pub, err := s.Run("redlink KEY SHOW")
	// if err != nil {
	// 	return err
	// }

	// TODO: We should send our host keys so they don't need to do a host scan.
	s.Commands = append(s.Commands,
		fmt.Sprintf("redlink HOST DEFINE %s %s", arbitraryHost, "localhost"),
		fmt.Sprintf("redlink HOST CONFIG PORT %s %s", arbitraryHost, arbitraryPort),
		fmt.Sprintf("redlink HOST CONFIG USER %s %s", arbitraryHost, r.CFG.User.Name),
		fmt.Sprintf("redlink LINK DIR %s %s %s", arbitraryHost, l.Remote, l.Local),
		fmt.Sprintf("redlink RELOAD"),
	)
	return nil
}

func (r *ReverseDir) Reload() error {
	r.Clean()

	for _, l := range r.CFG.Link.ReverseDir {
		if err := r.Link(l); err != nil {
			return fmt.Errorf("Link host %q: %v", l.Host.Name, err)
		}
	}

	if err := r.ConnectToHosts(); err != nil {
		return err
	}

	return nil
}

func (r *ReverseDir) Status() []string {
	lines := []string{}

	for _, l := range r.CFG.Link.ReverseDir {
		lines = append(lines, fmt.Sprintf("%s %s<<<%s",
			l.Host.Id,
			l.Local,
			l.Remote,
		))
	}

	return lines
}

func New(cfg *config.Config) (*ReverseDir, error) {
	r := &ReverseDir{CFG: cfg}
	r.port = port.New()
	return r, nil
}

package redlink

import (
	"fmt"
	"log"

	"gitlab.com/ilmikko/redlink/config"
	"gitlab.com/ilmikko/redlink/redlink/dir"
	"gitlab.com/ilmikko/redlink/redlink/keys"
	"gitlab.com/ilmikko/redlink/redlink/port"
	"gitlab.com/ilmikko/redlink/redlink/reversedir"
	"gitlab.com/ilmikko/redlink/redlink/reverseport"
)

type Redlink struct {
	CFG         *config.Config
	Keys        *keys.Keys
	Port        *port.Port
	ReversePort *reverseport.ReversePort
	Dir         *dir.Dir
	ReverseDir  *reversedir.ReverseDir
	Socket      *Socket
}

func (r *Redlink) Clean() {
	log.Printf("Cleanup...")
	r.Port.Clean()
	r.ReversePort.Clean()
	r.Dir.Clean()
	r.ReverseDir.Clean()
}

func (r *Redlink) Listen() error {
	if r.Socket == nil {
		return nil
	}
	return r.Socket.Listen()
}

func (r *Redlink) Reload() error {
	log.Printf("Reloading...")
	if err := r.CFG.Validate(); err != nil {
		return fmt.Errorf("Cannot reload because configuration is invalid: %v", err)
	}
	if err := r.Keys.Reload(); err != nil {
		return err
	}
	if err := r.Port.Reload(); err != nil {
		return err
	}
	if err := r.ReversePort.Reload(); err != nil {
		return err
	}
	if err := r.Dir.Reload(); err != nil {
		return err
	}
	if err := r.ReverseDir.Reload(); err != nil {
		return err
	}
	return nil
}

func (r *Redlink) Status() []string {
	lines := []string{
		fmt.Sprintf("Redlink %s@%s:%s",
			r.CFG.User.Name,
			r.CFG.Host.Name,
			r.CFG.Host.Port,
		),
	}

	lines = append(lines, r.Keys.Status()...)
	lines = append(lines, r.CFG.Status()...)
	lines = append(lines, r.Port.Status()...)
	lines = append(lines, r.ReversePort.Status()...)
	lines = append(lines, r.Dir.Status()...)
	lines = append(lines, r.ReverseDir.Status()...)

	return lines
}

func New(cfg *config.Config) (*Redlink, error) {
	rl := &Redlink{CFG: cfg}

	// Generate all trust keys first based on configuration.
	// This module overwrites most SSH configuration files under the link user.
	k, err := keys.New(cfg)
	if err != nil {
		return nil, err
	}

	p, err := port.New(cfg)
	if err != nil {
		return nil, err
	}

	rp, err := reverseport.New(cfg)
	if err != nil {
		return nil, err
	}

	d, err := dir.New(cfg)
	if err != nil {
		return nil, err
	}

	rd, err := reversedir.New(cfg)
	if err != nil {
		return nil, err
	}

	s, err := NewSocket(cfg, rl)
	if err != nil {
		return nil, err
	}

	rl.Keys = k
	rl.Port = p
	rl.ReversePort = rp
	rl.Dir = d
	rl.ReverseDir = rd
	rl.Socket = s

	return rl, nil
}

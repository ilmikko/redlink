package port

import (
	"fmt"

	"gitlab.com/ilmikko/redlink/common/ssh"
	"gitlab.com/ilmikko/redlink/config"
)

type PortLink struct {
	Local  string
	Remote string
}

func (pl *PortLink) String() string {
	return fmt.Sprintf("%s:localhost:%s", pl.Local, pl.Remote)
}

func NewPortLink(l *config.Link) *PortLink {
	pl := &PortLink{
		Local:  l.Local,
		Remote: l.Remote,
	}
	return pl
}

type Port struct {
	CFG  *config.Config
	SSHs map[string]*ssh.SSH
}

func (p *Port) Clean() {
	if p.SSHs != nil {
		for _, s := range p.SSHs {
			s.Disconnect()
		}
	}
	p.SSHs = map[string]*ssh.SSH{}
}

func (p *Port) ConnectToHosts() error {
	for _, s := range p.SSHs {
		if err := s.Connect(); err != nil {
			return err
		}
	}
	return nil
}

func (p *Port) Link(l *config.Link) error {
	if _, ok := p.SSHs[l.Host.Id]; !ok {
		p.SSHs[l.Host.Id] = ssh.New(p.CFG.Keys.PrivFile(), l.Host)
	}

	s := p.SSHs[l.Host.Id]
	s.Options = append(s.Options, "-L", NewPortLink(l).String())
	return nil
}

func (p *Port) Reload() error {
	p.Clean()

	for _, l := range p.CFG.Link.Port {
		if err := p.Link(l); err != nil {
			return fmt.Errorf("Link host %q: %v", l.Host.Name, err)
		}
	}

	if err := p.ConnectToHosts(); err != nil {
		return err
	}
	return nil
}

func (p *Port) Status() []string {
	lines := []string{}

	for _, l := range p.CFG.Link.Port {
		lines = append(lines, fmt.Sprintf("%s %s->%s",
			l.Host.Id,
			l.Local,
			l.Remote,
		))
	}

	return lines
}

func New(cfg *config.Config) (*Port, error) {
	p := &Port{CFG: cfg}
	return p, nil
}

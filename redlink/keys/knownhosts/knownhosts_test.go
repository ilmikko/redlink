package knownhosts_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"testing"

	knownhosts "."
	"gitlab.com/ilmikko/redlink/config"
)

func TestAddToKeySet(t *testing.T) {
	testHost := config.NewHost("test-host", "test-host")
	ks := knownhosts.NewKeySet(testHost)

	type addCase struct {
		keyType string
		key     string
	}

	testCases := []struct {
		want    *knownhosts.KeySet
		wantErr string
		add     addCase
	}{
		{
			add: addCase{
				keyType: "ssh-rsa",
				key:     "ssh-rsa-key",
			},
			want: &knownhosts.KeySet{
				Host: testHost,
				Keys: map[string]string{
					"ssh-rsa": "ssh-rsa-key",
				},
			},
		},
		{
			add: addCase{
				keyType: "ssh-ed25519",
				key:     "ssh-ed25519-key",
			},
			want: &knownhosts.KeySet{
				Host: testHost,
				Keys: map[string]string{
					"ssh-rsa":     "ssh-rsa-key",
					"ssh-ed25519": "ssh-ed25519-key",
				},
			},
		},
		{
			add: addCase{
				keyType: "ssh-rsa",
				key:     "conflicting-ssh-rsa-key",
			},
			wantErr: fmt.Sprintf(
				"Host %q key (ssh-rsa) mismatch!\nOLD: \"ssh-rsa-key\"\nNEW: \"conflicting-ssh-rsa-key\"",
				testHost.Id,
			),
			want: &knownhosts.KeySet{
				Host: testHost,
				Keys: map[string]string{
					"ssh-rsa":     "ssh-rsa-key",
					"ssh-ed25519": "ssh-ed25519-key",
				},
			},
		},
	}

	for _, tc := range testCases {
		err := ks.Add(tc.add.keyType, tc.add.key)
		if tc.wantErr != "" && err != nil {
			if got, want := fmt.Sprintf("%v", err), tc.wantErr; !strings.Contains(got, want) {
				t.Errorf("Error not as expected:\n got: %q\nwant: %q",
					got,
					want,
				)
			}
		} else if err != nil {
			t.Errorf("ks.Add returned an error that was not expected: %v", err)
		}

		if got, want := ks, tc.want; !reflect.DeepEqual(got, want) {
			t.Errorf("KeySet not as expected:\n got: %v\nwant: %v",
				got,
				want,
			)
		}
	}
}

func TestAddSet(t *testing.T) {
	kh := &knownhosts.KnownHosts{}
	kh.HostKeys = map[string]*knownhosts.KeySet{}

	firstHost := config.NewHost("first-host", "first-host")
	secondHost := config.NewHost("second-host", "second-host")

	type addCase struct {
		host   string
		keySet *knownhosts.KeySet
	}

	testCases := []struct {
		want    *knownhosts.KnownHosts
		wantErr string
		add     addCase
	}{
		{
			add: addCase{
				host: "first-host",
				keySet: &knownhosts.KeySet{
					Host: firstHost,
					Keys: map[string]string{},
				},
			},
			want: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"first-host": &knownhosts.KeySet{
						Host: firstHost,
						Keys: map[string]string{},
					},
				},
			},
		},
		{
			add: addCase{
				host: "second-host",
				keySet: &knownhosts.KeySet{
					Host: secondHost,
					Keys: map[string]string{
						"ssh-rsa": "ssh-rsa-key",
					},
				},
			},
			want: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"first-host": &knownhosts.KeySet{
						Host: firstHost,
						Keys: map[string]string{},
					},
					"second-host": &knownhosts.KeySet{
						Host: secondHost,
						Keys: map[string]string{
							"ssh-rsa": "ssh-rsa-key",
						},
					},
				},
			},
		},
		{
			add: addCase{
				host: "first-host",
				keySet: &knownhosts.KeySet{
					Host: firstHost,
					Keys: map[string]string{
						"ssh-rsa":     "ssh-rsa-key",
						"ssh-ed25519": "ssh-ed25519-key",
					},
				},
			},
			want: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"first-host": &knownhosts.KeySet{
						Host: firstHost,
						Keys: map[string]string{
							"ssh-rsa":     "ssh-rsa-key",
							"ssh-ed25519": "ssh-ed25519-key",
						},
					},
					"second-host": &knownhosts.KeySet{
						Host: secondHost,
						Keys: map[string]string{
							"ssh-rsa": "ssh-rsa-key",
						},
					},
				},
			},
		},
		{
			add: addCase{
				host: "second-host",
				keySet: &knownhosts.KeySet{
					Host: secondHost,
					Keys: map[string]string{
						"ssh-ed25519": "ssh-ed25519-key",
					},
				},
			},
			want: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"first-host": &knownhosts.KeySet{
						Host: firstHost,
						Keys: map[string]string{
							"ssh-rsa":     "ssh-rsa-key",
							"ssh-ed25519": "ssh-ed25519-key",
						},
					},
					"second-host": &knownhosts.KeySet{
						Host: secondHost,
						Keys: map[string]string{
							"ssh-rsa":     "ssh-rsa-key",
							"ssh-ed25519": "ssh-ed25519-key",
						},
					},
				},
			},
		},
		{
			add: addCase{
				host: "first-host",
				keySet: &knownhosts.KeySet{
					Host: firstHost,
					Keys: map[string]string{
						"ssh-ed25519": "conflicting-ssh-ed25519-key",
					},
				},
			},
			want: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"first-host": &knownhosts.KeySet{
						Host: firstHost,
						Keys: map[string]string{
							"ssh-rsa":     "ssh-rsa-key",
							"ssh-ed25519": "ssh-ed25519-key",
						},
					},
					"second-host": &knownhosts.KeySet{
						Host: secondHost,
						Keys: map[string]string{
							"ssh-rsa":     "ssh-rsa-key",
							"ssh-ed25519": "ssh-ed25519-key",
						},
					},
				},
			},
			wantErr: "Key migration failed",
		},
	}

	for _, tc := range testCases {
		err := kh.AddSet(tc.add.host, tc.add.keySet)
		if tc.wantErr != "" && err != nil {
			if got, want := fmt.Sprintf("%v", err), tc.wantErr; !strings.Contains(got, want) {
				t.Errorf("Error not as expected:\n got: %q\nwant: %q",
					got,
					want,
				)
			}
		} else if err != nil {
			t.Errorf("ks.Add returned an error that was not expected: %v", err)
		}

		if got, want := kh, tc.want; !reflect.DeepEqual(got, want) {
			t.Errorf("KeySet not as expected:\n got: %v\nwant: %v",
				got,
				want,
			)
		}
	}
}

func TestProjectMode(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestProjectMode")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testKnownHosts := fmt.Sprintf("%s/%s", dir, "known_hosts")

	kh := &knownhosts.KnownHosts{}
	kh.HostKeys = map[string]*knownhosts.KeySet{}
	if err := kh.Project(testKnownHosts); err != nil {
		t.Fatalf(".NewKnownHosts().Project(%q): %v", testKnownHosts, err)
	}

	stat, err := os.Stat(testKnownHosts)
	if err != nil {
		t.Errorf("stat(%q): %v", testKnownHosts, err)
	}

	want := "-rw-------"
	got := stat.Mode().String()
	if got != want {
		t.Errorf("File mode for %q not as expected!\n got: %v\nwant: %v", testKnownHosts, got, want)
	}
}

func TestProject(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestProject")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	someHost := config.NewHost("some-host", "some-host")
	alphaHost := config.NewHost("alpha-host", "alpha-host")
	betaHost := config.NewHost("beta-host", "beta-host")
	weirdHost := config.NewHost("weird-host", "weird-host")
	weirdHost.Port = "12345"
	testKnownHosts := fmt.Sprintf("%s/%s", dir, "known_hosts")

	testCases := []struct {
		knownHosts *knownhosts.KnownHosts
		want       string
	}{
		{
			knownHosts: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"some-host": &knownhosts.KeySet{
						Host: someHost,
						Keys: map[string]string{
							"ssh-rsa": "some-host-key key-comment",
						},
					},
				},
			},
			want: strings.Join(
				[]string{
					knownhosts.EDIT_WARNING,
					"some-host ssh-rsa some-host-key key-comment",
					"",
				}, "\n",
			),
		},
		{
			knownHosts: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"alpha-host": &knownhosts.KeySet{
						Host: alphaHost,
						Keys: map[string]string{
							"ssh-rsa": "alpha-host-key ALPHA IS FIRST",
						},
					},
					"beta-host": &knownhosts.KeySet{
						Host: betaHost,
						Keys: map[string]string{
							"ssh-rsa":     "beta-host-key BETA IS BEST",
							"ssh-ed25519": "beta-host-key-2 BETA IS SECOND",
						},
					},
				},
			},
			want: strings.Join(
				[]string{
					knownhosts.EDIT_WARNING,
					"alpha-host ssh-rsa alpha-host-key ALPHA IS FIRST",
					"beta-host ssh-ed25519 beta-host-key-2 BETA IS SECOND",
					"beta-host ssh-rsa beta-host-key BETA IS BEST",
					"",
				}, "\n",
			),
		},
		{
			knownHosts: &knownhosts.KnownHosts{
				HostKeys: map[string]*knownhosts.KeySet{
					"weird-host": &knownhosts.KeySet{
						Host: weirdHost,
						Keys: map[string]string{
							"ssh-rsa": "weird-host-key # weird ports are allowed!",
						},
					},
				},
			},
			want: strings.Join(
				[]string{
					knownhosts.EDIT_WARNING,
					"[weird-host]:12345 ssh-rsa weird-host-key # weird ports are allowed!",
					"",
				}, "\n",
			),
		},
	}

	for _, tc := range testCases {
		if err := tc.knownHosts.Project(testKnownHosts); err != nil {
			t.Errorf("Project returned an error: %v", err)
		}

		got, err := ioutil.ReadFile(testKnownHosts)
		if err != nil {
			t.Errorf("Cannot read file %q: %v", testKnownHosts, err)
		}

		if got, want := string(got), tc.want; got != want {
			t.Errorf("Known hosts %q not as expected:\n got: %q\nwant: %q",
				testKnownHosts,
				got,
				want,
			)
		}
	}
}

// Skipping this test as unix.OverrideExec is not a thing is this repository.

// func TestScan(t *testing.T) {
// 	dir, err := ioutil.TempDir("", "TestScan")
// 	if err != nil {
// 		t.Fatalf("Cannot make temporary directory: %v", err)
// 	}
// 	defer os.RemoveAll(dir)
//
// 	unix.OverrideExec(func(cmd string, args ...string) string {
// 		switch cmd {
// 		case "ssh-keyscan":
// 			host := args[0]
// 			return strings.Join([]string{
// 				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
// 				fmt.Sprintf("%s %s %s",
// 					host,
// 					"ssh-rsa",
// 					fmt.Sprintf("%s-key", host),
// 				),
// 			}, "\n")
// 		}
// 		return ""
// 	})
//
// 	scans := []struct {
// 		host string
// 		want *knownhosts.KeySet
// 	}{
// 		{
// 			host: "some-host",
// 			want: &knownhosts.KeySet{
// 				Host: "some-host",
// 				Keys: map[string]string{
// 					"ssh-rsa": "some-host-key",
// 				},
// 			},
// 		},
// 		{
// 			host: "other-host",
// 			want: &knownhosts.KeySet{
// 				Host: "other-host",
// 				Keys: map[string]string{
// 					"ssh-rsa": "other-host-key",
// 				},
// 			},
// 		},
// 	}
//
// 	for _, scan := range scans {
// 		ks := knownhosts.NewKeySet(scan.host)
// 		if err := ks.Scan(); err != nil {
// 			t.Errorf("Scan returned an error: %v", err)
// 		}
//
// 		if got, want := ks, scan.want; !reflect.DeepEqual(got, want) {
// 			t.Errorf("KeySet not as expected:\n got: %v\nwant: %v",
// 				got,
// 				want,
// 			)
// 		}
// 	}
// }

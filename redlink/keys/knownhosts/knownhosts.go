package knownhosts

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"sort"
	"strings"

	"gitlab.com/ilmikko/redlink/config"
)

const (
	MODE         = 0600
	EDIT_WARNING = "# AUTOMATICALLY GENERATED FILE BY REDLINK; DO NOT EDIT MANUALLY!"
)

type KeySet struct {
	Host *config.Host
	Keys map[string]string
}

func hostPort(host, port string) string {
	if port == "22" {
		return host
	}

	// Non-standard ports need to be designated in known_hosts.
	// See man sshd(8) for more information about the format.
	return fmt.Sprintf("[%s]:%s", host, port)
}

func (ks *KeySet) parseHostKeys(r io.Reader) error {
	s := bufio.NewScanner(r)
	for s.Scan() {
		fields := strings.Fields(s.Text())

		// Lines need to have at least hostname, key type and key data.
		if len(fields) < 3 {
			continue
		}

		// Comments
		if fields[0][0] == '#' {
			continue
		}

		hostName := fields[0]
		if hostName != hostPort(ks.Host.Name, ks.Host.Port) {
			log.Printf("Dropping unknown key result because hosts do not match (%q != %q): %s", hostName, ks.Host.Name, fields)
			continue
		}

		keyType := fields[1]
		keyData := strings.Join(fields[2:], " ")
		if err := ks.Add(keyType, keyData); err != nil {
			return err
		}
	}

	return nil
}

func (ks *KeySet) Add(keyType, key string) error {
	switch keyType {
	case "ssh-rsa", "ecdsa-sha2-nistp256", "ssh-ed25519":
	default:
		// TODO: Should this always return an error?
		return fmt.Errorf("Unknown key type: %q", keyType)
	}

	if oldKey, ok := ks.Keys[keyType]; ok {
		if oldKey != key {
			return fmt.Errorf("Host %q key (%s) mismatch!\nOLD: %q\nNEW: %q", ks.Host.Id, keyType, oldKey, key)
		}
		return nil
	}

	ks.Keys[keyType] = key
	return nil
}

func (ks *KeySet) Empty() bool {
	return len(ks.Keys) == 0
}

func (ks *KeySet) Scan() error {
	log.Printf("Scanning keys for host %q (%s)...", ks.Host.Id, ks.Host.Name)
	cmd := exec.Command("ssh-keyscan", "-p", ks.Host.Port, ks.Host.Name)

	var o, e bytes.Buffer
	cmd.Stdout = &o
	cmd.Stderr = &e

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("Key scanning failed for host %q: %v: %s", ks.Host.Id, err, e.String())
	}

	if err := ks.parseHostKeys(strings.NewReader(o.String())); err != nil {
		return fmt.Errorf("Error parsing host keys: %v", err)
	}

	log.Printf("Found %d keys for host %q.", len(ks.Keys), ks.Host.Id)
	return nil
}

func NewKeySet(host *config.Host) *KeySet {
	return &KeySet{
		Host: host,
		Keys: map[string]string{},
	}
}

type KnownHosts struct {
	CFG      *config.Config
	HostKeys map[string]*KeySet
}

func (kh *KnownHosts) AddEmpty(host *config.Host) {
	if _, ok := kh.HostKeys[host.Id]; !ok {
		kh.HostKeys[host.Id] = NewKeySet(host)
	}
}

func (kh *KnownHosts) Add(host *config.Host, kt, key string) error {
	kh.AddEmpty(host)
	return kh.HostKeys[host.Id].Add(kt, key)
}

func (kh *KnownHosts) AddSet(host string, keys *KeySet) error {
	log.Printf("Adding host %q with %d key(s)", host, len(keys.Keys))
	if oldKeys, ok := kh.HostKeys[host]; ok {
		for kt, k := range keys.Keys {
			if err := oldKeys.Add(kt, k); err != nil {
				return fmt.Errorf("Key migration failed: %v", err)
			}
		}
		return nil
	}

	kh.HostKeys[host] = keys
	return nil
}

func (kh *KnownHosts) Status() []string {
	lines := []string{}

	// Count key set sizes
	trustKeys := 0
	for _, keys := range kh.HostKeys {
		trustKeys += len(keys.Keys)
	}

	lines = append(lines, fmt.Sprintf("%d known host(s) with %d trusted key(s) [TK]", len(kh.HostKeys), trustKeys))

	return lines
}

func (kh *KnownHosts) String() string {
	s := []string{}

	for _, keys := range kh.HostKeys {
		for keyType, key := range keys.Keys {
			hp := hostPort(keys.Host.Name, keys.Host.Port)
			s = append(s, fmt.Sprintf("%s %s %s\n", hp, keyType, key))
		}
	}

	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })

	return strings.Join(s, "")
}

// Project to a known_hosts file.
func (kh *KnownHosts) Project(file string) error {
	f, err := os.OpenFile(file, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, MODE)
	if err != nil {
		return err
	}
	defer f.Close()

	fmt.Fprintf(f, "%s\n", EDIT_WARNING)
	fmt.Fprintf(f, "%s", kh.String())
	return nil
}

func (kh *KnownHosts) Reload() error {
	kh.HostKeys = map[string]*KeySet{}

	for _, h := range kh.CFG.Hosts {
		kh.AddEmpty(h)

		for kt, key := range h.HostKeys {
			if err := kh.Add(h, kt, key); err != nil {
				return err
			}
		}
	}

	if err := kh.ScanEmptyLinkedHosts(); err != nil {
		return fmt.Errorf("Scanning empty linked hosts: %v", err)
	}

	if err := kh.Project(kh.CFG.Keys.KnownHostsFile()); err != nil {
		return fmt.Errorf("Project: %v", err)
	}

	return nil
}

func (kh *KnownHosts) ScanEmptyLinkedHosts() error {
	for id, h := range kh.CFG.Hosts {
		if !h.Active {
			continue
		}

		ks := kh.HostKeys[id]
		if !ks.Empty() {
			continue
		}
		if err := ks.Scan(); err != nil {
			return err
		}
	}

	return nil
}

func New(cfg *config.Config) (*KnownHosts, error) {
	kh := &KnownHosts{CFG: cfg}
	return kh, nil
}

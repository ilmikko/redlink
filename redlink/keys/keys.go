package keys

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/ilmikko/redlink/config"
	"gitlab.com/ilmikko/redlink/redlink/keys/authkeys"
	"gitlab.com/ilmikko/redlink/redlink/keys/knownhosts"
)

const (
	KEY_BITS     = 2048
	PROFILE_MODE = 0644
)

type Keys struct {
	CFG        *config.Config
	AuthKeys   *authkeys.AuthKeys
	KnownHosts *knownhosts.KnownHosts
}

func (k *Keys) CreateProfile() error {
	f, err := os.OpenFile(k.CFG.User.Profile(), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, PROFILE_MODE)
	if err != nil {
		return err
	}
	defer f.Close()

	fmt.Fprintf(f, "PATH=$PATH:%s\n", k.CFG.User.Bin())
	return nil
}

func (k *Keys) Generate(path string) error {
	log.Printf("Generating key %q", path)
	cmd := exec.Command("ssh-keygen",
		fmt.Sprintf("-b %d", KEY_BITS),
		"-N", "",
		"-f", path,
	)

	var b bytes.Buffer
	cmd.Stdout = &b
	cmd.Stderr = &b

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("ssh-keygen for %q: %v: %s", path, err, b.String())
	}
	return nil
}

func (k *Keys) LocalKeyGenerate() error {
	priv := k.CFG.Keys.PrivFile()
	if _, err := os.Stat(priv); os.IsNotExist(err) {
		if err := k.Generate(priv); err != nil {
			return err
		}
	}
	log.Printf("Using local key %q", priv)
	return nil
}

// TODO: Cache this?
func (k *Keys) Public() (string, error) {
	pub := k.CFG.Keys.PubFile()
	b, err := ioutil.ReadFile(pub)
	if err != nil {
		return "", err
	}
	return strings.Trim(string(b), " \n"), nil
}

func (k *Keys) Reload() error {
	if err := k.AuthKeys.Reload(); err != nil {
		return err
	}
	if err := k.KnownHosts.Reload(); err != nil {
		return err
	}
	return nil
}

func (k *Keys) Status() []string {
	lines := []string{}

	lines = append(lines, k.KnownHosts.Status()...)
	lines = append(lines, k.AuthKeys.Status()...)

	return lines
}

func New(cfg *config.Config) (*Keys, error) {
	k := &Keys{
		CFG: cfg,
	}

	{
		kh, err := knownhosts.New(cfg)
		if err != nil {
			return nil, fmt.Errorf("Known hosts: %v", err)
		}

		k.KnownHosts = kh
	}

	{
		ak, err := authkeys.New(cfg)
		if err != nil {
			return nil, fmt.Errorf("Authorized keys: %v", err)
		}

		k.AuthKeys = ak
	}

	if err := k.LocalKeyGenerate(); err != nil {
		return nil, fmt.Errorf("Failed to generate local keys: %v", err)
	}

	if err := k.CreateProfile(); err != nil {
		return nil, fmt.Errorf("Failed to generate profile: %v", err)
	}

	return k, nil
}

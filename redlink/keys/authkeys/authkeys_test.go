package authkeys_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	authkeys "."
)

func TestProjectMode(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestProjectMode")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testAuthKeys := fmt.Sprintf("%s/%s", dir, "authorized_keys")

	ak := authkeys.AuthKeys{}
	if err := ak.Project(testAuthKeys); err != nil {
		t.Fatalf("AuthKeys.Project(%q): %v", testAuthKeys, err)
	}

	stat, err := os.Stat(testAuthKeys)
	if err != nil {
		t.Errorf("stat(%q): %v", testAuthKeys, err)
	}

	want := "-rw-------"
	got := stat.Mode().String()
	if got != want {
		t.Errorf("File mode for %q not as expected!\n got: %v\nwant: %v", testAuthKeys, got, want)
	}
}

func TestProject(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestProject")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testAuthKeys := fmt.Sprintf("%s/%s", dir, "authorized_keys")

	testCases := []struct {
		authKeys *authkeys.AuthKeys
		want     string
	}{
		{
			authKeys: &authkeys.AuthKeys{
				Keys: map[string]string{
					"some-host": "some-host-key # key-comment",
				},
			},
			want: strings.Join([]string{
				authkeys.EDIT_WARNING,
				"some-host-key # key-comment # Host \"some-host\"",
				"",
			}, "\n"),
		},
	}

	for _, tc := range testCases {
		if err := tc.authKeys.Project(testAuthKeys); err != nil {
			t.Errorf("Project returned an error: %v", err)
		}

		got, err := ioutil.ReadFile(testAuthKeys)
		if err != nil {
			t.Errorf("Cannot read file %q: %v", testAuthKeys, err)
		}

		if got, want := string(got), tc.want; got != want {
			t.Errorf("Known hosts %q not as expected:\n got: %q\nwant: %q",
				testAuthKeys,
				got,
				want,
			)
		}
	}
}

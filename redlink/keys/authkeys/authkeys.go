package authkeys

import (
	"fmt"
	"os"
	"sort"
	"strings"

	"gitlab.com/ilmikko/redlink/config"
)

const (
	MODE = 0600
	// TODO: Instead, where to edit?
	EDIT_WARNING = "# AUTOMATICALLY GENERATED FILE BY REDLINK; DO NOT EDIT MANUALLY!"
)

type AuthKeys struct {
	CFG  *config.Config
	Keys map[string]string
}

func (ak *AuthKeys) Add(host, key string) error {
	if oldKey, ok := ak.Keys[host]; ok {
		if oldKey != key {
			return fmt.Errorf("Host %q auth key mismatch!\nOLD: %q\nNEW: %q", host, oldKey, key)
		}
		return nil
	}

	ak.Keys[host] = key
	return nil
}

func (ak *AuthKeys) Project(file string) error {
	f, err := os.OpenFile(file, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, MODE)
	if err != nil {
		return err
	}
	defer f.Close()

	fmt.Fprintf(f, "%s\n", EDIT_WARNING)
	fmt.Fprintf(f, "%s", ak.String())
	return nil
}

func (ak *AuthKeys) Reload() error {
	ak.Keys = map[string]string{}

	for _, h := range ak.CFG.Hosts {
		if h.AuthKey == "" {
			continue
		}

		ak.Add(h.Name, h.AuthKey)
	}

	if err := ak.Project(ak.CFG.Keys.AuthKeysFile()); err != nil {
		return fmt.Errorf("Project: %v", err)
	}

	return nil
}

func (ak *AuthKeys) Status() []string {
	lines := []string{}

	lines = append(lines, fmt.Sprintf("%d active authorized key(s) [AK]", len(ak.Keys)))

	return lines
}

func (ak *AuthKeys) String() string {
	s := []string{}

	for host, key := range ak.Keys {
		s = append(s, fmt.Sprintf("%s # Host %q\n", key, host))
	}

	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })

	return strings.Join(s, "")
}

func New(cfg *config.Config) (*AuthKeys, error) {
	ak := &AuthKeys{CFG: cfg}
	return ak, nil
}

package dir

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"syscall"
	"time"

	"gitlab.com/ilmikko/redlink/config"
)

const (
	// How long, roughly, do we wait before declaring a new dir link dead?
	DIR_CONNECT_TIMEOUT = 5 * time.Second
	// How many health checks should we make in that time?
	DIR_CONNECT_TRIES = 5
	// How many seconds to wait after SIGTERM before sending SIGKILL to disconnect SSHFS?
	DIR_DISCONNECT_TIMEOUT = 5 * time.Second
	// How long to wait before starting health check?
	SSHFS_INIT_WAIT = 2 * time.Second
)

type DirLink struct {
	CFG    *config.Config
	Cmd    *exec.Cmd
	Link   *config.Link
	Stderr *bytes.Buffer
	Stdout *bytes.Buffer
}

func (dl *DirLink) Connect() error {
	h := dl.Link.Host
	log.Printf("Dir link to %q (%s)...", h.Id, h.Name)
	dl.Disconnect()

	dl.Cmd = exec.Command(
		"sshfs",
		"-o", strings.Join(dl.CFG.User.SSHFSOptions, ","),
		"-p", h.Port,
		"-o", fmt.Sprintf("IdentityFile=%s",
			dl.CFG.Keys.PrivFile(),
		),
		fmt.Sprintf("%s@%s:%s",
			h.User,
			h.Name,
			dl.Link.Remote,
		),
		dl.Link.Local,
	)

	var o, e bytes.Buffer
	dl.Cmd.Stdout = &o
	dl.Cmd.Stderr = &e
	dl.Stdout = &o
	dl.Stderr = &e

	if err := dl.Cmd.Start(); err != nil {
		return fmt.Errorf("Connect start: %v", err)
	}

	time.Sleep(SSHFS_INIT_WAIT)

	// We will check that the link actually connects.
	for i := 0; i < DIR_CONNECT_TRIES; i++ {
		if dl.Healthy() {
			break
		}
		time.Sleep(DIR_CONNECT_TIMEOUT / DIR_CONNECT_TRIES)
	}

	if !dl.Healthy() {
		return fmt.Errorf("Host %q did not connect in time. STDERR: %s", h.Id, dl.Stderr.String())
	}
	log.Printf("Host %q connected successfully.", h.Id)

	return nil
}

func (dl *DirLink) Disconnect() error {
	// Unmount any existing connections from the directory.
	c := exec.Command("fusermount", "-u", dl.Link.Local)
	if err := c.Run(); err != nil {
		return err
	}

	if dl.Cmd == nil {
		return nil
	}
	if err := dl.Cmd.Process.Kill(); err != nil {
		return err
	}
	time.Sleep(DIR_DISCONNECT_TIMEOUT)
	if err := dl.Cmd.Process.Signal(syscall.SIGKILL); err != nil {
		return err
	}
	return nil
}

func (dl *DirLink) Healthy() bool {
	// Output to STDERR is always registered unhealthy.
	if dl.Stderr.String() != "" {
		return false
	}

	// TODO: Add an uplink link-socket kind of structure here
	return true
}

func NewDirLink(cfg *config.Config, l *config.Link) *DirLink {
	dl := &DirLink{CFG: cfg}
	dl.Link = l

	return dl
}

type Dir struct {
	CFG      *config.Config
	DirLinks map[string][]*DirLink
}

func (d *Dir) Clean() {
	if d.DirLinks != nil {
		for _, dls := range d.DirLinks {
			for _, dl := range dls {
				dl.Disconnect()
			}
		}
	}
	d.DirLinks = map[string][]*DirLink{}
}

func (d *Dir) ConnectToHosts() error {
	for _, dls := range d.DirLinks {
		for _, dl := range dls {
			if err := dl.Connect(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (d *Dir) Link(l *config.Link) error {
	dls := d.DirLinks[l.Host.Id]

	d.DirLinks[l.Host.Id] = append(dls, NewDirLink(d.CFG, l))
	return nil
}

func (d *Dir) Reload() error {
	// TODO: Be smarter about this. If host configuration has not changed, we don't need to disconnect that specific host.
	d.Clean()

	for _, l := range d.CFG.Link.Dir {
		if err := d.Link(l); err != nil {
			return err
		}
	}

	if err := d.ConnectToHosts(); err != nil {
		return err
	}

	return nil
}

func (d *Dir) Status() []string {
	lines := []string{}

	for _, l := range d.CFG.Link.Dir {
		lines = append(lines, fmt.Sprintf("%s %s>>>%s",
			l.Host.Id,
			l.Local,
			l.Remote,
		))
	}

	return lines
}

func New(cfg *config.Config) (*Dir, error) {
	d := &Dir{CFG: cfg}
	return d, nil
}

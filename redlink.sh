#!/usr/bin/env bash
# Simple Redlink installation/configuration script.
# Hopefully this will give an idea how to configure Redlink for an arbitrary UNIX host.

LINKUSER="link";
LINKHOME="/link";
LINKBIN="$LINKHOME/bin";

KEYDIR=".ssh";

INSTALLDIR="$LINKHOME/redlink";

CONFIG="$LINKHOME/redlink.conf";
SERVICE="redlink.service";

fail() {
	echo "$@" 1>&2
	exit 1;
}

root() {
	sudo "$@";
}

echo "Installing Redlink...";

# Add user and its home directory.
id "$LINKUSER" || root useradd "$LINKUSER" || fail "Failed to add user '$LINKUSER'!";
root mkdir -p "$LINKHOME" || fail "Failed to add home dir '$LINKHOME'!";

# Add key directory.
root mkdir -p "$LINKHOME/$KEYDIR" || fail "Failed to add keydir '$LINKHOME/$KEYDIR'!";
root chmod 700 "$LINKHOME/$KEYDIR" || fail "Failed to chmod keydir '$LINKHOME/$KEYDIR'!";

# Clone the repo to the install directory.
if [ ! -d "$INSTALLDIR" ]; then
	tmp="$(mktemp --directory)";
	git clone "https://gitlab.com/ilmikko/redlink.git" "$tmp" || fail "Git cloning failed.";
	root mv "$tmp" "$INSTALLDIR";
	root chmod 755 "$INSTALLDIR";
fi
# Update git repo, removing any local changes.
(
root chown -R "$USER:$USER" "$INSTALLDIR";
cd "$INSTALLDIR";
git reset --hard || fail "Git repository cannot be reset.";
git pull || fail "Cannot update git repository.";
root chown -R "$LINKUSER:$LINKUSER" "$INSTALLDIR";
)

# Create config.
cat <<HERE | root tee "$CONFIG"
USER BIN $INSTALLDIR/bin
USER NAME $LINKUSER
KEY DIR $KEYDIR
HERE

# Create service file.
cat <<HERE | root tee "/etc/systemd/system/$SERVICE"
[Unit]
Description=Redlink daemon
Before=multi-user.target

[Service]
WorkingDirectory=$LINKHOME
User=$LINKUSER
Type=simple
Restart=on-failure
RestartSec=2s
ExecStartPre=/usr/bin/make --directory="$INSTALLDIR"
ExecStart=$INSTALLDIR/bin/redlinkd --config "$CONFIG"

[Install]
WantedBy=multi-user.target
HERE

# Start service.
root systemctl daemon-reload;
root systemctl restart "$SERVICE" || fail "Service startup failed.";
root systemctl enable "$SERVICE" || fail "Service enable failed.";
